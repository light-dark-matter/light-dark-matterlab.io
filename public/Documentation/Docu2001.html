<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Documentation for version 2.001</title>
    <link rel="stylesheet" href="../style.css">
    <link rel="shortcut icon" href="../icons/dire-logo-3.png"/>
  </head>
  <body>

    <h1>Documentation for version 2.001</h1>

    <p>
      Thanks for your interest in DIRE (short for dipole resummation), a
      <code>C++</code> program for all-order radiative
      corrections to scattering processes in high-energy particle collisions.
      This page collects the information on input keyword settings as used  by
      versions 2.001 and before. 

    <h2>Input settings</h2>

    <p>
      The Dire plugin for Pythia 8 is implemented in accorrdance with the rules
      of implementing new parton showers for Pythia 8. Thus, the code
      structure is very similar to Pythia 8. Also, (almost) all input settings
      are reclycled from Pythia 8. In the following, we will first document
      Dire-specific input settings and then discuss the code structure.
    </p>

    <p>
      Dire currently has only very few switches, so that a very sophisticated
      documentation is not yet necessary. Below, we list some Dire-specific
      handles for quick reference. <i>All other settings are taken from
      Pythia 8, keeping their meaning. The Pythia 8 documentation can be found
      on the <a href="http://home.thep.lu.se/~torbjorn/Pythia.html">Pythia
      homepage</a>.</i>
    </p>

    <p>
      Settings related to the accuracy of splitting kernels used in the
      evolution:
    </p>

    <ul>
      <li>
        <b>DireTimes:kernelOrder = n</b> : <i>n</i> is an integer value, and
        set to 1 by default. This setting defines which higher-order
        corrections are applied to the parton-shower splitting functions used
        for timelike (i.e. final state) evolution:
        <ul>
          <li>
            <i>n=0</i>: Leading order evolution kernels, as defined in the
            original Dire publication. 
          </li>
          <li>
            <i>n=1</i>: Leading order evolution kernels, and additional
            rescaling of the soft pieces to incorporate two-loop cusp efects
            (similar to the CMW scheme).
          </li>
          <li>
            <i>n=2</i>: Leading order evolution kernels, and additional
            rescaling of the soft pieces to incorporate two- and three-loop
            cusp efects.
          </li>
          <li>
            <i>n=3</i>: Leading order evolution kernels, additional rescaling
            of the soft pieces   to incorporate two- and three-loop cusp
            efects, and NLO corrections to collinear evolution from NLO DGLAP
            kernels. 
          </li>
        </ul>
      </li>

      <li>
        <b>DireSpace:kernelOrder = n</b> : <i>n</i> is an integer value, and
        set to 1 by default. This setting defines which higher-order
        corrections are applied to the parton-shower splitting functions used
        for spacelike (i.e. initial state) evolution:
        <ul>
          <li>
            <i>n=0</i>: Leading order evolution kernels, as defined in the
            original Dire publication. 
          </li>
          <li>
            <i>n=1</i>: Leading order evolution kernels, and additional
            rescaling of the soft pieces to incorporate two-loop cusp efects
            (similar to the CMW scheme).
          </li>
          <li>
            <i>n=2</i>: Leading order evolution kernels, and additional
            rescaling of the soft pieces to incorporate two- and three-loop
            cusp efects.
          </li>
          <li>
            <i>n=3</i>: Leading order evolution kernels, additional rescaling
            of the soft pieces   to incorporate two- and three-loop cusp
            efects, and NLO corrections to collinear evolution from NLO DGLAP
            kernels. 
          </li>
        </ul>
      </li>
    </ul>

    <p>
      Settings related to the evaluation of running (QCD) couplings:
    </p>

    <ul>
      <li>
        <b>ShowerPDF:usePDFalphas = on/off</b> : This switch is turned off
        by default. Turned on, the alphaS running and thresholds will be
        directly taken from the PDF set interfaced through LHAPDF6. This can be
        helpful when validating the code, but will lead to a longer run time.
      </li>
    </ul>

    <p>
      Settings to perform variations to gauge shower uncertainties:
    </p>

    <ul>
      <li>
        <b>Variations:doVariations = on/off</b> : This switch is turned off by
        default. Turned on, this means that the timelike and spacelike showers
        are allowed to perform on-the-fly variations of the renormalization
        scale. Renormalization scale uncertainties are an important part of the
        perturbative evolution. Note that currently, these variations will also
        apply to showers off (soft) secondary scatterings. The range of
        renormalization scale variations is given by the next four parameters.
      </li>

      <li>
        <b>Variations:muRisrDown = k</b> : The (double) value with which the
        (GeV<sup>2</sup>-valued) argument of &alpha;<sub>s</sub> in
        initial-state evolution should be rescaled to produce a smaller value
        of the renormalization scale in the context of automatic variations.
      </li>

      <li>
        <b>Variations:muRisrUp = k</b> : The (double) value with which the
        (GeV<sup>2</sup>-valued) argument of &alpha;<sub>s</sub> in
        initial-state evolution should be rescaled to produce a larger value
        of the renormalization scale in the context of automatic variations.
      </li>

      <li>
        <b>Variations:muRfsrDown = k</b> : The (double) value with which the
        (GeV<sup>2</sup>-valued) argument of &alpha;<sub>s</sub> in
        final-state evolution should be rescaled to produce a smaller value
        of the renormalization scale in the context of automatic variations.
      </li>

      <li>
        <b>Variations:muRfsrUp = k</b> : The (double) value with which the
        (GeV<sup>2</sup>-valued) argument of &alpha;<sub>s</sub> in
        final-state evolution should be rescaled to produce a larger value
        of the renormalization scale in the context of automatic variations.
      </li>
    </ul>

    <p>
      Settings related to parton distribution functions and quark masses:
    </p>

    <ul>
      <li>
        <b>ShowerPDF:usePDFmasses = on/off</b> : This switch is turned on by
        default. Turned on, quark masses will be directly taken from the PDF
        set interfaced through LHAPDF6. 
      </li>

      <li>
        <b>ShowerPDF:useSummedPDF = on/off</b> : This switch is turned on by
        default. Turned on, this means that the PDF ratios that are used in the
        evolution once an initial state parton partakes in a branching include
        both sea and valence quark contributions (if applicable).
      </li>

      <li>
        <b>DireSpace:useGlobalMapIF = on/off</b> : This switch is turned off by
        default. Turned on, this means that the phase space of intial state
        emissions with a final state spectator is setup such that all final
        state particles share the momentum recoil of the emission.
      </li>

      <li>
        <b>DireSpace:forceMassiveMap = on/off</b> : This switch is turned off
        by default. Turned on, this means that initial state emissions are
        allowed masses. This means the shower produces the awkward situation
        that incoming quarks are massless, but yield a assive final state
        quark upon conversion to an incoming gluon.
      </li>
    </ul>

    <p>
      Settings related to the tune of Pythia 8 + Dire:
    </p>

    <ul>
      <li>
        <b>Dire:Tune = n</b> : <i>n</i> is an integer value, and is set to 1
        by default. If set to one, this enables the default tune of
        Pythia8 + Dire. Currently, all other values mean that no Pythia
        parameters are automatically overwritten by Dire.
      </li>
    </ul>

    <p>
      Settings for debugging or educational purposes:
    </p>

    <ul>
      <li>
        <b>DireSpace:nFinalMax = n</b> : <i>n</i> is an integer value, and set
        to -10 by default. The spacelike showers will stop if this number of
        final state particles is reached.
      </li>

      <li>
        <b>DireTimes:nFinalMax = n</b> : <i>n</i> is an integer value, and set
        to -10 by default. The timelike showers will stop if this number of
        final state particles is reached.
      </li>
    </ul>

    <p><i>
      Please note that all other settings are taken from Pythia 8, keeping
      their meaning. The Pythia 8 documentation can be found on the
      <a href="http://home.thep.lu.se/~torbjorn/Pythia.html">Pythia
      homepage</a>.
    </i></p>

    <h2>Program flow</h2>

    <p>
      Dire closely follows the rules outlined in the "Implement New Showers"
      section of the
      <a href="http://home.thep.lu.se/~torbjorn/pythia82html/Welcome.html">
      Pythia manual</a>. The evolution thus proceeds roughly as follows:
    </p>

    <ol>
      <li>
        Initialise the shower with the <code>init</code> function.
      </li>
      <li>
        Set up the state for showering with the <code>prepare</code> and
        <code>setupQCDdip</code> functions. If this is not the first shower
        step, the function <code>update</code> will be used instead of
        <code>prepare</code>.
      </li>
      <li>
        Propose the next evolution step with <code>pTnext</code> (both in ISR
        and FSR). Through this function, the shower has already picked
        probabilistically among all possibly branchings, using the Sudakov
        veto algorithm. Pythia 8 then decides if this evolution step wins
        out over multiparton interactions.
      </li>
      <li>
        If the Dire step has not been picked, then restart the evolution of the
        new state at the evolution scale at which Pythia 8 decided to change
        the state, i.e. continue from step 2.
      </li>
      <li>
        If the Dire step has been chosen, generate the new state by inferring
        the <code>branch</code> function.
      </li>
      <li>
        Restart from step 2.
      </li>
    </ol>

    <p>
      One main construction difference is the way that branching functions are
      handled. For maximal modularity, no overestimates or splitting kernels
      are hard-coded in the timelike or spacelike showers. Instead, Dire loads
      a library of splitting kernel objects upon initialisation. The default
      splitting library used in Dire is assembled in
      <code>SplittingLibrary.cc</code>, which uses Splitting class objects as
      an input. All massless and massive QCD splitting kernels are available
      in <code>SplittingsQCD.cc</code>. New splitting kernels can be
      implemented in Dire by deriving from the <code>Splitting</code> class
      defined in <code>Splittings.h</code>.
    </p>

    <p>
      Note further that Dire works with a weighted parton shower, as some
      splitting kernels are not positive definite in the soft gluon limit.
      This means that after showering, events may contain a non-unit weight.
      This weight is handled in the <code>WeightContainer</code> class.
      The necessary weight that has to be used for histogramming can be
      obtained from the <code>getShowerWeight()</code> function of the
      <code>WeightContainer</code> class.
    </p>

    <script src="../app.js"></script>
  </body>
</html>