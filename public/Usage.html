<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Usage</title>
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="icons/dire-logo-3.png"/>
  </head>
  <body>

    <h1>Usage</h1>

    Here, some basic and a bit more advanced Dire usage (QED, merging, 
    matrix-element corrections -- see below) is discussed. 

    <h2>Basic usage</h2>

    Dire inherits its usage philosophy from Pythia: The idea is that the user
    should write small C++ programs that steer the event generation. Various
    examples are collected in the <code>main</code> directory of the
    distribution:
    <ul>
      <li>
      <code>dire-viz.cc</code>: This main program mainly serves educational
      purposes. With it, you can render a three-dimensional "movie" of a
      scattering event, that can be viewed with a standard HTML browser.
      </li>
      <li>
      <code>dire00.cc</code>: This simple main program illustrates how
      you can run Dire and internally analyze the output events by using
      Pythia's internal jet finders.
      </li>
      <li>
      <code>dire01.cc</code>: This is again a very simple main program, designed
      to illustrate how to produce <a href="http://lcgapp.cern.ch/project/simu/HepMC/">HepMC2</a> output files, possibly using input
      Les Houches Event files (e.g. produced by <a href="https://launchpad.net/mg5amcnlo">MadGraph</a>
      or <a href="https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.7.html">Sherpa</a>)
      </li>
      <li>
      <code>dire02.cc</code>: This main program extends <code>dire01.cc</code>
      by also setting up the structures necessary for ME+PS merging (including
      what would be necessary for matrix element corrections).
      </li>
      <li>
      <code>dire03.cc</code>: This program illustrates how to run Dire,
      use parton shower scale variations and produce one HepMC output file
      for each variation. 
      </li>
      <li>
      <code>dire04.cc</code>:  This is again a very simple main program, designed
      to illustrate how to produce <a href="https://promc.hepforge.org/">ProMC</a> output files.
      </li>
      <li>
      <code>dire05.cc</code>: This main program illustrates how one could systematize an internal analyis of Dire outputs.
      </li>
    </ul>

    <p>
    The main program <code>dire.cc</code> is instead intended to allow users
    to steer Dire with input files and terminal instructions instead, and is 
    thus somewhat more complicated. This program (which is also installed to
    <code>bin/dire</code> upon executing <code>make install</code>) can act
    as a Dire main program without the user having to write their own
    steering program -- so let's discuss its usage here.
    </p>

    <p>
    You can access the documentation of this program by executing
    </p>
    <p>
    <code>./bin/dire --help</code>
    </p>
    <p>
    The most common use will be to supply a file to read all input settings from
    by using the <code>--input</code> command line option. Another useful
    feature is OpenMP parallelization, which can be activated by specifying
    <code>--nthreads</code>. Note that for the latter, Dire has to be configured
    with OpenMP support (see <code>./configure --help</code>). Another useful
    command line option is to specify HepMC2 output through 
    <code>--hepmc_output</code>. Finally, all Pythia settings can by supplied
    as command line options through the <code>--setting</code> keyword.
    Some example usage is
    </p>
    <p>
    <code>./bin/dire --nevents 50 --setting "WeakSingleBoson:ffbar2gmZ = on"</code>
    </p>
    <p>
    <code>./bin/dire --input main/lep.cmnd --hepmc_output myfile.hepmc</code>
    </p>

    <h2>QED showers</h2>

    QED showers are in principle no different from any other Pythia or Dire
    settings, and thus do not really require non-basic usage. Here's an 
    example input file to use QED showers in hadron collider events:

    <pre>
	# Number of events to generate.
	Main:numberOfEvents       = 10000
	# LHC beam setup
	Beams:idA                 = 2212
	Beams:idB                 = 2212
	Beams:eCM                 = 7000.
	# Example: Neutral-current Drell-Yan e+e- production
	WeakSingleBoson:ffbar2gmZ = on
	23:onMode                 = off
	23:onIfAny                = 13 -13 
	PhaseSpace:mHatMin        = 20.
	# Do not use "power shower".
	SpaceShower:pTmaxMatch    = 1
	TimeShower:pTmaxMatch     = 1
	# Set QED cut-offs and switch on QED shower.
	TimeShower:pTminChgQ = 0.0001
	TimeShower:pTminChgL = 0.0001
	SpaceShower:pTminChgQ = 0.001
	SpaceShower:pTminChgL = 0.001
	TimeShower:QEDshowerByQ = on
	TimeShower:QEDshowerByL = on
	SpaceShower:QEDshowerByQ = on
	SpaceShower:QEDshowerByL = on
	# Below this pT value, only retain positiv electric charge correlators.
	# This improves the numerical stability, but should be reassessed if
	# other values have significant impact on your favorite observables.
	Dire:QED:pTminAllowNeg = 0.2
    </pre>

    <h2>Using merging</h2>

    ME+PS merging allows to correct the parton shower approximation for the
    spectrum of multiple emissions with more accurate fixed-order
    results. Multi-emission configurations with different multiplicty can
    be corrected simultaneously. For example, if the lowest multiplicity
    scattering is A + B &rarr; C, then merging allows to produce an 
    event sample in which the processes A + B &rarr; C,
    A + B &rarr; C + 1 hard emission, A + B &rarr; C + 2 hard emissions,
    A + B &rarr; C + 3 hard emission, and so on, are all described 
    with fixed-order (e.g. tree-level or NLO) accuracy. What is meant by "hard
    emission" depends on the event generator. Dire allows to treat QCD partons,
    photons, leptons and W- or Z-bosons as additional hard emissions. As
    such, it extends the electroweak merging of  
    <a href="https://arxiv.org/abs/1510.01517">arxiv:1510.01517 [hep-ph]</a>
    to photons, leptons and Z-bosons. Electroweak showers are approximated
    in the scheme of 
    <a href="https://arxiv.org/abs/1403.4788">arxiv:1403.4788 [hep-ph]</a>.

    <p>
    The merging capabilities of Dire are illustrated with the 
    <code>dire02</code> example program. We advise using/expanding this 
    example. <code>dire02</code> takes two arguments: First the settings
    file, and second the name of an output HepMC event file. The available
    settings are very similar to the example settings file
    <code>main89ckkwl.cmnd</code> and friends
    of the standard Pythia distribution. These are documented in detail in the
    <a href="http://home.thep.lu.se/~torbjorn/pythia82html/Welcome.html">Pythia online documentation</a>.
    Some small sample input Les Houches Event files are included in the
    Dire distribution in the <code>include/DirePlugins/mg5</code> directory.
    This directory further includes matrix element correction codes.
    It is strongly recommended to link Dire against a suitable suite of
    matrix elements, to ensure consistency of the merging. An example 
    configure step would be
    </p>

    <p>
    <code>./configure --with-pythia8=/path/to/pythia --with-mg5mes=$(pwd)/include/DirePlugins/mg5 --mg5mes-model=sm</code>
    </p>

    <p>
    before compiling Dire and before compiling your main program.
    A functional input settings file for leading-order merging could look like
    this:
    </p>

    <pre>
	Main:numberOfEvents = 10000
	Beams:frameType     = 4

	! Wimpy shower
	TimeShower:pTmaxMatch  = 1
	SpaceShower:pTmaxMatch = 1

	! Use same alpha_s value as in ME calculation
	SpaceShower:alphaSvalue   = 0.1203
	TimeShower:alphaSvalue    = 0.1203
	! Use same PDF set as in ME calculation (note: requires to link LHAPDF6)
	!PDF:pSet                  = LHAPDF6:CT14nnlo
	!PDF:pHardSet              = LHAPDF6:CT14nnlo

	! Specify merging parameters for CKKW-L, UMEPS, UNLOPS.
	! Merging scale definition: pT of jets w.r.t. beam
	Merging:unlopsTMSdefinition    = 1
	Merging:doUserMerging          = on

	! Merging scale value
	Merging:TMS                    = 20. 
	! Maximal number of additional LO jets
	Merging:nJetMax                = 2   
	! Use Pythia factorisation/renormalisation scales for 
	! lowest-multiplicity process
	Merging:usePythiaQRenHard      = on
	Merging:usePythiaQFacHard      = on
	! Allow any process with jets and W- or Z-bosons as
	! lowest-multiplicity process
	Merging:Process                = pp>Zinc,Winc,Jinc
	Merging:mayRemoveDecayProducts = on
	! Switch on electroweak PS & merging.
	SpaceShower:EWshowerByQ        = on  
	TimeShower:EWshowerByQ         = on
	! Use collinear kT as argument of alphaS
	DireSpace:alphasScheme         = 2          
	DireTimes:alphasScheme         = 2

	! Be more forgiving with momentum mismatches.
	Check:epTolErr                 = 2e-2
	LesHouches:setQuarkMass        = 0
	LesHouches:setLeptonMass       = 0
	LesHouches:matchInOut          = off
	ShowerPDF:usePDFmasses         = off
	1:m0 = 0.0
	2:m0 = 0.0
	3:m0 = 0.0
	4:m0 = 0.0
	5:m0 = 0.0

	! Madgraph settings to let ME corrections know about MG5 parameters.
	Dire:MG5card  = ../include/DirePlugins/mg5/Processes_sm/param_card_sm.dat
	SLHA:file     = ../include/DirePlugins/mg5/Processes_sm/param_card_sm.dat

	! Switch off event generation steps
	!PartonLevel:MPI              = off
	!HadronLevel:all              = off
	!BeamRemnants:primordialKT    = off
	!PartonLevel:Remnants         = off
	!Check:event                  = off

	! Subruns for CKKW-L merging
	Main:numberOfSubruns = 3
	Main:subrun               = 0
	Beams:LHEF                = ../include/DirePlugins/mg5/e-ve_lo_0.lhe.gz
	Main:subrun               = 1
	Beams:LHEF                = ../include/DirePlugins/mg5/e-ve_lo_1.lhe.gz
	Main:subrun               = 2
	Beams:LHEF                = ../include/DirePlugins/mg5/e-ve_lo_2.lhe.gz
    </pre>

    <p>
    You can save this to a file (say, <code>mergingLO.cmnd</code>) and then
    run
    <p>
    <code>./dire02 mergingLO.cmnd mergingLO_output.hepmc</code>
    </p>
    </p>


    <p>
    NLO merging input settings are not much more complicated. An
    example could look like this (note that only the last bit is different from
    the LO example above):
    </p>

    <pre>
	Main:numberOfEvents = 10000
	Beams:frameType     = 4

	! Wimpy shower
	TimeShower:pTmaxMatch  = 1
	SpaceShower:pTmaxMatch = 1

	! Use same alpha_s value as in ME calculation
	SpaceShower:alphaSvalue   = 0.1203
	TimeShower:alphaSvalue    = 0.1203
	! Use same PDF set as in ME calculation (note: requires to link LHAPDF6)
	!PDF:pSet                  = LHAPDF6:CT14nnlo
	!PDF:pHardSet              = LHAPDF6:CT14nnlo

	! Specify merging parameters for CKKW-L, UMEPS, UNLOPS.
	! Merging scale definition: pT of jets w.r.t. beam
	Merging:unlopsTMSdefinition    = 1

	! Merging scale value
	Merging:TMS                    = 20. 
	! Maximal number of additional LO jets
	Merging:nJetMax                = 2   
	! Maximal number of additional NLO jets
	Merging:nJetMaxNLO             = 1
	! Use Pythia factorisation/renormalisation scales for 
	! lowest-multiplicity process
	Merging:usePythiaQRenHard      = on
	Merging:usePythiaQFacHard      = on
	! Allow any process with jets and W- or Z-bosons as
	! lowest-multiplicity process
	Merging:Process                = pp>Zinc,Winc,Jinc
	Merging:mayRemoveDecayProducts = on
	! Switch on electroweak PS & merging.
	SpaceShower:EWshowerByQ        = on  
	TimeShower:EWshowerByQ         = on
	! Use collinear kT as argument of alphaS
	DireSpace:alphasScheme         = 2          
	DireTimes:alphasScheme         = 2

	! Be more forgiving with momentum mismatches.
	Check:epTolErr                 = 2e-2
	LesHouches:setQuarkMass        = 0
	LesHouches:setLeptonMass       = 0
	LesHouches:matchInOut          = off
	ShowerPDF:usePDFmasses         = off
	1:m0 = 0.0
	2:m0 = 0.0
	3:m0 = 0.0
	4:m0 = 0.0
	5:m0 = 0.0

	! Madgraph settings to let ME corrections know about MG5 parameters.
	Dire:MG5card  = ../include/DirePlugins/mg5/Processes_sm/param_card_sm.dat
	SLHA:file     = ../include/DirePlugins/mg5/Processes_sm/param_card_sm.dat

	! Switch off event generation steps
	!PartonLevel:MPI              = off
	!HadronLevel:all              = off
	!BeamRemnants:primordialKT    = off
	!PartonLevel:Remnants         = off
	!Check:event                  = off

	! Factorisation/renormalisation scales core process
	Merging:muFac               = 80.4
	Merging:muRen               = 80.4
	Merging:muFacInME           = 80.4
	Merging:muRenInME           = 80.4

	! Subruns for UNLOPS NLO merging
	Main:numberOfSubruns = 4

	Main:subrun               = 0
	Merging:doUNLOPSTree      = off
	Merging:doUNLOPSSubt      = off
	Merging:doUNLOPSLoop      = on
	Merging:doUNLOPSSubtNLO   = off
	Beams:LHEF                = ../include/DirePlugins/mg5/e-ve_nlo_0.lhe.gz

	Main:subrun               = 1
	Merging:doUNLOPSTree      = off
	Merging:doUNLOPSSubt      = off
	Merging:doUNLOPSLoop      = on
	Merging:doUNLOPSSubtNLO   = off
	Beams:LHEF                = ../include/DirePlugins/mg5/e-ve_nlo_1.lhe.gz

	Main:subrun               = 2
	Merging:doUNLOPSTree      = on
	Merging:doUNLOPSSubt      = off
	Merging:doUNLOPSLoop      = off
	Merging:doUNLOPSSubtNLO   = off
	Beams:LHEF                = ../include/DirePlugins/mg5/e-ve_lo_1.lhe.gz

	Main:subrun               = 3
	Merging:doUNLOPSTree      = on
	Merging:doUNLOPSSubt      = off
	Merging:doUNLOPSLoop      = off
	Merging:doUNLOPSSubtNLO   = off
	Beams:LHEF                = ../include/DirePlugins/mg5/e-ve_lo_2.lhe.gz
    </pre>

    <p>
    You can save this to a file (say, <code>mergingNLO.cmnd</code>) and then
    run
    <p>
    <code>./dire02 mergingNLO.cmnd mergingNLO_output.hepmc</code>
    </p>
    </p>

    <p>
    Before moving on, a few comments about input sample generation are in 
    order. The general strategy is that the user can produce input events with
    include cuts, and then Dire will apply the necessary stronger cuts 
    based on the merging scale definition automatically. The merging
    scale choice for Dire is very simple, and easily applicable also in 
    NLO fixed-order calculations: Each sample should have a minimal number
    of jets with transverse momentum (relative to the beam direction) above
    the merging scale value. The longitudinally invariant kT algorithm with
    radius R=0.4 and maximal jet rapidity of 6.0 is used to define jets.
    Some (pseudo)code for enforcing this merging scale might best explain 
    this definition:
    </p>

    <pre>
	// Fastjet analysis - select algorithm and parameters
	fastjet::Strategy               strategy = fastjet::Best;
	fastjet::RecombinationScheme    recombScheme = fastjet::E_scheme;
	fastjet::JetDefinition         *jetDef
	   = new fastjet::JetDefinition(fastjet::kt_algorithm, Rparam,
	     recombScheme, strategy);
	// Fastjet input
	std::vector &lt; fastjet::PseudoJet &gt; fjInputs;

	// Loop over event record to decide what to pass to FastJet
	for (int i = 0; i &lt; event.size(); ++i) {
	  // (Final state && coloured+photons) only!
	  if ( !event[i].isFinal()
	    || event[i].isLepton()
	    || event[i].id() == 23
	    || abs(event[i].id()) == 24)
	    continue;
	  // Store as input to Fastjet
	  fjInputs.push_back( fastjet::PseudoJet (event[i].px(),
	          event[i].py(), event[i].pz(),event[i].e() ) );
	}

	// Run Fastjet algorithm
	fastjet::ClusterSequence clusterSeq(fjInputs, *jetDef);
	vector&lt;fastjet::PseudoJet&gt; jets
	  = fastjet::sorted_by_pt(clusterSeq.inclusive_jets());
	int n(0);
	for (size_t i(0);i&lt;jets.size();++i) {
	  Vec4 pj(jets[i].px(),jets[i].py(),jets[i].pz(),jets[i].E());
	  if (pj.pT()&gt; ptmin && abs(pj.rap()) &lt; ymax) n++;
	}

	// Veto if too few jets have been found.
	if (n &lt; number_of_partons_in_born) vetoThisFixedOrderEvent;
    </pre>

    <p>
    This allows to generate leading order LHE samples without any Dire-specific
    cuts. MadGraph could be used for these samples.

    The cut also allows to use, for example, aMC@NLO-generated NLO events that were
    regularised with that code's <code>ptjet</code> cut.
    aMC@NLO-generated NLO events will at present have to be post-processed to
    work with Dire. One option is to generate aMC@NLO events with PYTHIA 8
    subtractions, then post-process them with PYTHIA 8 (using consistent 
    settings), and store the events after the first PYTHIA 8 emission to LHEF. 
    The resulting event files can then be used as NLO input samples for Dire.
    A dedicated code for post-processing aMC@NLO inputs with PYTHIA 8 can be
    found 
    <a href="Downloads/pythia-for-amcatnlo-postprocessing-FAKE.tar.gz">here</a>.
    This has been developed by Leif Gellersen.
    If you need more details on merging in Dire, please contact Stefan 
    Prestel at stefanDOTprestelATthepDOTluDOTse (replace DOT and AT
    with the appropriate symbol).
    </p>

    <h2>Using iterated matrix-element corrections</h2>

    The first step to using Dire with matrix-element corrections is to link
    the code against a suitable suite of ME corrections. An example 
    configure step would be
    </p>

    <p>
    <code>./configure --with-pythia8=/path/to/pythia --with-mg5mes=$(pwd)/include/DirePlugins/mg5 --mg5mes-model=sm</code>
    </p>

    <p>
    before compiling Dire and before compiling your main program.
    Please note the description in the UpdateHistory.txt file in
    the 
    <a href="https://gitlab.com/dire/direforpythia">Dire repository gitlab.com/dire/direforpythia</a>
    for more details on how to link Madgraph-generated matrix element code.
    Once you've linked the ME code, you can use ME corrections for all processes
    available from the library. Note that ME corrections can apply to any type
    of shower splitting, i.e. QCD, QED and EW branchings.

    You do not need a specific main program to use matrix element corrections.
    One option is to use the default main program <code>dire.cc</code>.
    A functional input settings file for 
    iterated matrix-element corrections could look like
    this:
    </p>

    <pre>
	# Number of events to generate.
	Main:numberOfEvents       = 10000

	# Set alphaS(mZ) value and running.
	SpaceShower:alphaSvalue   = 0.1201
	TimeShower:alphaSvalue    = 0.1201
	SpaceShower:alphaSorder   = 2
	TimeShower:alphaSorder    = 2

	# LHC beam setup
	Beams:idA                 = 2212
	Beams:idB                 = 2212
	Beams:eCM                 = 7000.
	# Example: Neutral-current Drell-Yan e+e- production
	WeakSingleBoson:ffbar2gmZ = on
	23:onMode                 = off
	23:onIfAny                = 13 -13 
	PhaseSpace:mHatMin        = 20.

	# Do not use "power shower".
	SpaceShower:pTmaxMatch    = 1
	TimeShower:pTmaxMatch     = 1

	# Use masses of PDF sets also for shower evolution.
	# Note: Only correctly handled when using external PDF sets from LHAPDF
	ShowerPDF:usePDFalphas     = off
	ShowerPDF:useSummedPDF     = on
	ShowerPDF:usePDFmasses     = off

	# Madgraph settings to let ME corrections know about MG5 parameters.
	Dire:MG5card   = ./include/DirePlugins/mg5/Processes_sm/param_card_sm.dat
	SLHA:file      = ./include/DirePlugins/mg5/Processes_sm/param_card_sm.dat
	SLHA:readFrom  = 2
	SLHA:keepSM    = off
	SLHA:minMassSM = 0.3

        # Matrix element correction settings
	Dire:doMECs     = on
	Merging:Process = pp>mu+mu-
        # Final-state multiplicty until which ME corrections should apply
	#Dire:nFinalMaxMECs = 3
        # Virtuality of any combination of two particles, above which to 
	# apply ME corrections.
	Dire:QminMECs  = 0.5
        # Evolution variable of branchings, above which to apply MECs.
	Dire:pTminMECs = 0.1
    </pre>

    <p>
    You can save this to a file (say, <code>mecs-example.cmnd</code>) and then
    run the command
    <p>
    <code>./bin/dire --input mecs-example.cmnd --hepmc_output myfile.hepmc</code>
    </p>
    </p>

    <p>
    The Dire repository only contains a small matrix element library.
    More matrix elements can be found at
    <a href="https://gitlab.com/dire/direforpythia_mg5_mes">https://gitlab.com/dire/direforpythia_mg5_mes</a>
    or may be generated with 
    <a href="Downloads/MG5_aMC_v2_6_0_WithPY8MEsPlugini-FAKE.tar.gz">this MadGraph 
    version</a> developed by Valentin Hirschi, based on previous work
    by Olivier Mattelaer and Johan Alwall.
    If you need more details on matrix element corrections in Dire, please 
    contact Stefan Prestel at stefanDOTprestelATthepDOTluDOTse (replace DOT and AT
    with the appropriate symbol).
    </p>

    <script src="app.js"></script>
  </body>
</html>
