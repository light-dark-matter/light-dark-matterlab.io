#!/bin/bash

set_environment () {

  echo " Set environment variables"

  INSTALLD="$1"
  VERSION="$2"

}

run () {

  mkdir -p $INSTALLD
  cd $INSTALLD

  wget https://zlib.net/fossils/zlib-${VERSION}.tar.gz

  echo " Unpack zlib"
  tar xvzf zlib-${VERSION}.tar.gz

  echo " Enter zlib directory"
  cd zlib-${VERSION}/

  echo " Configure zlib"
  ./configure --prefix=$INSTALLD

  echo " Compile zlib"
  make

  echo " Install zlib"
  make install

  echo " Finished zlib installation"

}

set_environment "$@"
run "$@"
