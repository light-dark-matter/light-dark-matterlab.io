<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Docker</title>
    <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="icons/dire-logo-3.png"/>
  </head>
  <body>

    <h1>Docker container for Pythia 8 + Dire</h1>

    <p>
      A fully working Pythia 8 + Dire docker container is available from
      docker hub. The current version is 1.1.0 and uses Pythia 8.235 and
      Dire 2.002. It inherits from the
      <a href="https://professor.hepforge.org/trac/wiki/docker">Professor
      container</a> version 2.2.2, which, in turn, inherits from the
      <a href="https://rivet.hepforge.org/trac/wiki/Docker">Rivet
      container</a> version 2.5.4.
    </p>

    <p>
      Installation instructions for docker can be found 
      <a href="https://docs.docker.com/engine/installation">here</a>.
      On Linux systems, create a group "docker" and add yourself to it to
      avoid having to run all docker commands as sudo.<br>
      <code>sudo groupadd docker</code> <br>
      <code>sudo usermod -aG docker $USER</code>
    </p>

    <h2>Obtaining the docker image</h2>

    <p>
      Simply execute <br>
      <code>docker pull pythiatools/pythiadire:1.1.0</code> <br>
      to download and store the docker container in
      <code>/var/lib/docker</code>.
    </p>

    <h2>Running the container</h2>

    <p>
      In the following we use the shorthand <code>docker run</code> to mean
      running the container with the following options <br>
      <code>docker run --rm -u `id -u $USER` -v $PWD:$PWD -w $PWD</code>. <br>
      The option <code>-v</code> will mount your current
      working directory <code>$PWD</code> to a container
      directory with the same name. This allows the container to access your
      local files and to write files to the current working directory.
    </p>

    <h3>Jupyter notebooks</h3>

    <p>
      One of the main purposes of the container is to give an introduction
      to the Pythia8 and Dire event generator framework. The container
      contains a few Jupyter notebooks, where each cell contains a couple of
      lines of Python code and can be executed by pressing SHIFT+ENTER.
    </p>

    <p>
      Note that some notebooks attempt to show pdfs. Depending on your
      browser, this might not work, as enabling plugins inside a sandboxed
      frame is not allowed. However, you can still view the pdf locally on
      your machine.
    </p>

    <p>
      To work with the notebooks, simply execute the command <br>
      <code>docker run -p 8888:8888 pythiatools/pythiadire:1.1.0</code> <br>
      to start the docker container. It will create the directory
      <code>$PWD/notebooks</code> on the host system,
      where all notebooks of the container will be copied into. If
      the directory is already present and contains a notebook named as a
      notebook present in the container, the corresponding notebook will
      not be copied. This way, you are able to store the status of the
      notebooks and re-use them at a later point.
    </p>

    <p>
      The terminal will display a link you can copy to a browser and it will
      redirect to a list of Jupyter notebooks and a README file, which 
      contains information about the different notebooks. Note: if you 
      encounter a <code>This site can't be reached</code>
      the link is of the form <code>http://10715c9c96bd:8888/?token=...</code>
      (where <code>...</code> is a lengthy combination of
      letters and numbers), replace it with
      <code>http://localhost:8888/?token=...</code> and try again.
    </p>

    <p>
      If you want to use custom Rivet analyses and or LHAPDF sets, simply
      store the relevant information in <code>/some/path</code>.
      The container will automatically detect source files of Rivet
      analyses and compile them into a Rivet library.
    </p>

    <h3>Event Generator + Rivet</h3>

    <p>
      As an alternative to the Jupyter notesbooks above, you can run Pythia8
      or Dire and Rivet directly via <br>
      <code>docker run pythiatools/pythiadire:1.1.0 eventGeneratorRun
      info</code> <br>
      where <code>info</code> is a plain text file, with
      exactly 6 lines. An example looks like this:
    </p>

    <p class="txtfile">
      dire <br>
      test.cmnd <br>
      1000 <br>
      hepmctest <br>
      test.yoda <br>
      ALICE_2016 ATLAS_2011_S8971293
    </p>

    <p>
      The first line indicates the generator to be used, valid options are
      "pythia" or "dire", followed by the settings file to be used. The third
      line specifies the number of events. The fourth and fifth line specify
      the name of the fifo, to which the HepMC events are written, and the
      name of the resulting yoda-file respectively. Finally, the last line is
      a comma and/or whitespace separated list of Rivet analyses.
    </p>

    <p>
      All Rivet analyses in your current working directory
      <code>$PWD</code> will be compiled to a Rivet library
      and any present LHAPDF PDF sets can be used as well.
    </p>

    <p>
      Instead of specifying a file, you can also give all input as command
      line parameters. Remember to put quotes around the list of Rivet
      analyses if it contains any whitespaces. Here is the command for
      the above example, <br>
      <code>docker run pythiatools/pythiadire:1.1.0
      eventGeneratorRun dire test.cmnd 1000
      hepmctest test.yoda "ALICE_2016 ATLAS_2011_S8971293"</code>.
    </p>

    <p>
      To run <code>rivet-mkhtml</code> with custom Rivet
      analyses, we provide a wrapper that takes as the first command line
      argument a directory, where the custom Rivet analyses are found on
      your local machine (has to be <code>$PWD</code> or a
      subdirectory), followed by the usual input for Rivet. Here is an
      example, <br>
      <code>docker run pythiatools/pythiadire:1.1.0
      rivet-mkhtml-custom $PWD/analyses --mc-errs out1.yoda out2.yoda
      </code>.
    </p>

    <h3>Event Generator Tuning</h3>

    <p>
      The container also provides some facilities for tuning with Professor
      and Rivet. It essentially automates what is done in the Jupyter
      notebook on tuning. There you can also find examples for the files
      on which is the tuning is based. The command is <br>
      <code>docker run pythiatools/pythiadire:1.1.0 tuning
      parameterRangesFile templateFile weightsFile limitsFile
      nPoints polynomialOrder runCombinations
      mcDirectory bestTuneDirectory
      runCombinationsFile ipolBase tuneBase
      runMCfile runIpolFile runTuneFile runBestTuneFile</code>, <br>
      where the first four command line arguments are the input files
      needed for the tuning: a file containing the ranges of the parameters,
      a template command file for Pythia 8 / Dire, a file for the weights,
      and a file for the limits during tuning (all to be found in your
      current working directory <code>$PWD</code>).
      The next three arguments give additional input for the tuning:
      the number of parameter points to sample, the run combinations to
      produce, and the order of the polynomial used for the interpolation.
      The next two command line arguments are the name of the directory
      in which the sampled parameter points and Monte Carlo runs should
      be stored and the name of the directory where the results for the
      best tune should be stored. The last seven arguments specifiy
      the output files produced by the tuning: the filename for the run
      combinations, the basename for the interpolation and tuning files,
      and the filenames for the scripts to produce the Monte Carlo runs, the
      interpolation, the tuning, and setting up the best tune (all will be
      found in your current working directory
      <code>$PWD</code> upon execution). Here is a concrete example, <br>
      <code>docker run pythiatools/pythiadire:1.1.0 tuning
      ranges.dat template.cmnd weights.dat limits.dat
      100 3 "0:1 1:10"
      mc bestTune
      runcombs.dat interpolation/ipol tuning/tune
      runMC.sh runIpol.sh runTune.sh runBestTune.sh</code>.<br>
      The files <code>runMC.sh</code>,
      <code>runIpol.sh</code>, and 
      <code>runTune.sh</code>,
      <code>runBestTune.sh</code> can be used to perform the
      more computationally intense tasks on a cluster.
      The file <code>runMC.sh</code> can be used for
      producing the Monte Carlo runs with the container as described in
      the Event Generator + Rivet section.
      The file <code>runIpol.sh</code> can be used to
      perform the interpolation for all run combinations.
      The file <code>runTune.sh</code> can be used to
      perform the tuning for all run combinations.
      The file <code>runBestTune.sh</code> can be used to
      find the best tune, get the eigentunes, and perform the relevant
      Monte Carlo runs.
      You have to modify the scripts according to your needs, for instance
      add commands to send jobs to a cluster.
    </p>

    <h3>Rivet and Professor</h3>

    <p>
      In addition you can use the container to run Rivet as described
      <a href="https://rivet.hepforge.org/trac/wiki/Docker">here</a> and
      Professor as described
      <a href="https://professor.hepforge.org/trac/wiki/docker">here</a>.
    </p>

    <script src="app.js"></script>
  </body>
</html>