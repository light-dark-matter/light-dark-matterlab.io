rm myhepmcfile.hepmc
mkfifo myhepmcfile.hepmc

DIRE_PATH=/path/to/dire
RIVET_PATH=/path/to/rivet
source $RIVET_PATH/rivetenv.sh

dire_exec_string="$DIRE_PATH/bin/dire --input $DIRE_PATH/main/lep.cmnd --nevents 1000 --setting 'HadronLevel:all = off' --hepmc_output myhepmcfile.hepmc"
rivet_exec_string="rivet -H myyodafile.yoda -a MC_XS myhepmcfile.hepmc"

echo "Run Dire with $dire_exec_string"
echo "Run Rivet with $rivet_exec_string"

$dire_exec_string | tee logfile 2>&1 &
$rivet_exec_string  > /dev/null 2>&1 &

wait
rm myhepmcfile.hepmc
