#!/bin/bash

set_environment () {

  echo "-- Set environment variables -----------------" | tee -a install.log 2>&1

  # These are the inputs you have to set!

  TOOLS="$(pwd)"
  BOOST_VERSION="1.55.0"
  LHAPDF_VERSION="6.1.5"
  ZLIB_VERSION="1.2.8"
  HEPMC_VERSION="2.06.09"
  PY_VERSION="8235"
  DIRE_VERSION="2.003"

  echo "Installation directory = $TOOLS" | tee -a install.log 2>&1

  echo "Boost version          = $BOOST_VERSION" | tee -a install.log 2>&1
  echo "zlib version           = $ZLIB_VERSION" | tee -a install.log 2>&1
  echo "LHAPDF version         = $LHAPDF_VERSION" | tee -a install.log 2>&1
  echo "HepMC version          = $HEPMC_VERSION" | tee -a install.log 2>&1
  echo "Pythia version         = $PY_VERSION" | tee -a install.log 2>&1
  echo "Dire version           = $DIRE_VERSION" | tee -a install.log 2>&1
  echo ""

}

run () {

 mkdir -p $TOOLS

 echo "" | tee -a install.log 2>&1
 echo "-- Begin installing BOOST --------------------" | tee -a install.log 2>&1
 wget https://dire.gitlab.io/Downloads/installBOOST.sh | tee -a install.log 2>&1
 chmod +x installBOOST.sh
 ./installBOOST.sh $TOOLS/BOOST $BOOST_VERSION | tee -a install.log 2>&1

 echo "" | tee -a install.log 2>&1
 echo "-- Begin installing LHAPDF --------------------" | tee -a install.log 2>&1
 wget https://dire.gitlab.io/Downloads/installLHAPDF6.sh | tee -a install.log 2>&1
 chmod +x installLHAPDF6.sh
 ./installLHAPDF6.sh $TOOLS/BOOST $TOOLS/LHAPDF6 $LHAPDF_VERSION | tee -a install.log 2>&1

 echo "" | tee -a install.log 2>&1
 echo "-- Begin installing HEPMC ---------------------" | tee -a install.log 2>&1
 wget https://dire.gitlab.io/Downloads/installHEPMC2.sh | tee -a install.log 2>&1
 chmod +x installHEPMC2.sh
 ./installHEPMC2.sh $TOOLS/HEPMC2 $HEPMC_VERSION | tee -a install.log 2>&1

 echo "" | tee -a install.log 2>&1
 echo "-- Begin installing ZILB ----------------------" | tee -a install.log 2>&1
 wget https://dire.gitlab.io/Downloads/installZLIB.sh | tee -a install.log 2>&1
 chmod +x installZLIB.sh
 ./installZLIB.sh $TOOLS/ZLIB $ZLIB_VERSION | tee -a install.log 2>&1

 echo "" | tee -a install.log 2>&1
 echo "-- Begin installing PYTHIA --------------------" | tee -a install.log 2>&1
 wget https://dire.gitlab.io/Downloads/installPYTHIA8.sh | tee -a install.log 2>&1
 chmod +x installPYTHIA8.sh

 if [[ "$PY_VERSION" -lt "8212" ]] ; then
  echo "Sorry, DIRE is only compatible with Pythia versions after 8.212" | tee -a install.log 2>&1
  echo "Continue installation without DIRE :(" | tee -a install.log 2>&1
 fi

 ./installPYTHIA8.sh $TOOLS/HEPMC2 $TOOLS/ZLIB $TOOLS/BOOST \
                     $TOOLS/LHAPDF6 $TOOLS/PYTHIA8 $PY_VERSION | tee -a install.log 2>&1

 echo "" | tee -a install.log 2>&1
 echo "-- Begin installing DIRE ----------------------" | tee -a install.log 2>&1
 wget https://dire.gitlab.io/Downloads/installDIRE.sh | tee -a install.log 2>&1
 chmod +x installDIRE.sh
 ./installDIRE.sh $TOOLS/PYTHIA8 $PY_VERSION $DIRE_VERSION | tee -a install.log 2>&1

}

echo "" | tee -a install.log 2>&1
echo "-----------------------------------------------" | tee -a install.log 2>&1
echo "-----------------------------------------------" | tee -a install.log 2>&1
echo "----- This script installs the HEP tools: -----" | tee -a install.log 2>&1
echo "--- BOOST, LHAPDF, ZLIB, HEPMC, PYTHIA, DIRE --" | tee -a install.log 2>&1
echo "-----------------------------------------------" | tee -a install.log 2>&1
echo "" | tee -a install.log 2>&1
echo " NOTE THAT THIS IS IN NO WAY A MINIMAL" | tee -a install.log 2>&1
echo " INSTALLATION." | tee -a install.log 2>&1
echo " THIS SCRIPT IS DESIGNED FOR CONVENIENCE ONLY." | tee -a install.log 2>&1
echo "" | tee -a install.log 2>&1
echo "-----------------------------------------------" | tee -a install.log 2>&1
echo "-----------------------------------------------" | tee -a install.log 2>&1

set_environment
run "$@"
 
echo "" | tee -a install.log 2>&1
echo "-- Finished installing HEP tools --------------" | tee -a install.log 2>&1
echo "-----------------------------------------------" | tee -a install.log 2>&1
echo "-----------------------------------------------" | tee -a install.log 2>&1

